##  Installation Using Docker

### 1. Setup .env file
```
cp ENV.example.php .ENV change values
```

### 2. Build application
```
docker-compose build
```

### 3. install composer
```
docker-compose run --rm app sh -c 'composer install'
```

### 4. Run application

```
docker-compose up -d
```

##access service on url: http://localhost or http://127.0.0.1

 http://127.0.0.1/getListTransactions
 http://127.0.0.1/getBlockCount
 http://127.0.0.1/getBalance
 http://127.0.0.1/getTransactionById 
 
 #2 methods: withdraw, getAddress 
 return an exception because the API and interface does not match
http://127.0.0.1/withdraw   
http://127.0.0.1/getAddress
