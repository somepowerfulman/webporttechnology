<?php
require(__DIR__ . "/../vendor/autoload.php");
$client = new \App\Classes\BTCClass();
$uri = trim($_SERVER['REQUEST_URI'], '/');

switch ($uri) {
    case '':
        header("Location: http://127.0.0.1/getListTransactions");
        break;
    case 'getBalance':
        dd($client->getBalance(6, true));
        break;
    case 'getTransactionById':
        dd($client->getTransactionById("fe4a242c91e87edff9d750ea05a4534739432a9361f28a6559bb4f38d856cb03"));
        break;
    case 'getBlockCount':
        dd($client->getBlockCount(), "getBlockCount");
        break;
    case 'withdraw':
        dd($client ->withdraw("1M72Sfpbz1BPpXFHz9m3CdqATR44Jvaydd", 0.1, "donation"));
        break;
    case 'getAddress':
        dd($client ->getAddress());
        break;
    case 'getListTransactions':
        dd($client ->getListTransactions("*", 20));
        break;
    default:
        header("HTTP/1.1 404 Not Found");
        exit();
}


function dd($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
