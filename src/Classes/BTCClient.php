<?php


namespace App\Classes;


class BTCClient
{
    private $baseUrl;
    private $password;
    private $defaultParams = ["jsonrpc" => "1.0", "id" => "curltest"];

    public function __construct($baseUrl, $password)
    {

        $this->baseUrl = $baseUrl;
        $this->password = $password;
    }

    private function makeRequest($data, $method = 'POST')
    {


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->baseUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'content-type: text/plain;',
                'Authorization: Basic ' . $this->password
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    /**
     * @param $params array
     * @return string
     */
    public function makePayload($params)
    {
        return json_encode(array_merge($this->defaultParams, $params));
    }

    /**
     * @param $minconf int
     * @param $include_watchonly bool
     * @@return array($result, $error, $id)
     */
    public function getBalance(int $minconf = 0, bool $include_watchonly = false): array
    {
        $params = [
            "method" => "getbalance",
            "params" => ["*", $minconf, $include_watchonly]
        ];
        $payload = $this->makePayload($params);
        return json_decode($this->makeRequest($payload), true);
    }


    /**
     * @param string $tabby
     * @return array
     */
    public function getAddress(string $tabby): array
    {
        $params = [
            "method" => "getaddressesbylabel",
            "params" => [$tabby]
        ];
        $payload = $this->makePayload($params);
        $responce = json_decode($this->makeRequest($payload), true);
        return $responce;
    }

    /**
     * @param string $label
     * @param int $count
     * @param int $skip
     * @param bool $include_watchonly
     * @return array($result, $error, $id)
     * )
     */
    public function getListTransactions($label = '*', $count = 10, $skip = 0, $include_watchonly = false)
    {
        $params = [
            "method" => "listtransactions",
            "params" => [$label, $count, $skip, $include_watchonly]
        ];
        $payload = $this->makePayload($params);
        return json_decode($this->makeRequest($payload), true);
    }

    /**
     * @param string $txid
     * @param bool $include_watchonly
     * @return array($result, $error, $id)
     */
    public function getTransactionById(string $txid, $include_watchonly = false): array
    {
        $params = [
            "method" => "gettransaction",
            "params" => [$txid, $include_watchonly]
        ];
        $payload = $this->makePayload($params);
        return json_decode($this->makeRequest($payload), true);

    }

    public function withdraw(string $address, float $amount, string $comment = null)
    {
        $params = [
            "method" => "sendtoaddress",
            "params" => [$address, $amount, $comment]
        ];
        $payload = $this->makePayload($params);
        return json_decode($this->makeRequest($payload), true);
    }

    /**
     * @return array($result, $error, $id)
     */
    public function getBlockCount()   //ok
    {
        $params = [
            "method" => "getblockcount"
        ];
        $payload = $this->makePayload($params);
        return json_decode($this->makeRequest($payload), true);
    }


}
