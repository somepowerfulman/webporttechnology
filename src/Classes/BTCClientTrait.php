<?php


namespace App\Classes;

trait BTCClientTrait
{
protected $btcClient = null;
public function getBTCClient(){
    require (__DIR__."/../../ENV.php");
    if ($this->btcClient === null){
        $this->btcClient = new BTCClient(BASE_URL, PASSWORD);
    }
    return $this->btcClient;
}

}