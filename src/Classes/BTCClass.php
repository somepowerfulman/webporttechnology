<?php

namespace App\Classes;

use App\Interfaces\Coin;


class BTCClass implements Coin
{
    use BTCClientTrait;

    public function __construct()
    {
    }

    /**
     * @inheritDoc
     */
    public function getAddress(): string
    {
        $responce = $this->getBTCClient()->getAddress('tabby');
        if (isset($responce["result"])) {
            return $responce["result"];
        } else {
            return $responce["error"]["message"];
        }
    }

    /**
     * @inheritDoc
     */
    public function getListTransactions(string $address = '*', int $block_number = 0): array
    {
        $responce = $this->getBTCClient()->getListTransactions($address, $block_number);

        $responce = $this->mapingResponceFields($responce);
        return $responce;
    }

    /**
     * @inheritDoc
     */
    public function getBalance(string $address = ''): float
    {
        $responce = $this->getBTCClient()->getBalance();
        return $responce["result"];

    }

    /**
     * @inheritDoc
     */
    public function withdraw(string $address, float $amount, string $describe = null): string
    {
        $responce = $this->getBTCClient()->withdraw($address, $amount, $describe);
        if (isset($responce["result"])) {
            return '$responce["result"]';
        } else {
            return $responce["error"]["message"];
        }
    }

    /**
     * @inheritDoc
     */
    public function getTransactionById(string $txid): array
    {
        $responce = $this->getBTCClient()->getTransactionById($txid);
        $responce = $this->mapingResponceFields($responce);
        return $responce;
    }

    /**
     * @return int
     */
    public function getBlockCount(): int
    {
        $responce = $this->getBTCClient()->getBlockCount();
        return $responce['result'];
    }

    /**
     * @param $responce array
     * @return array
     */
    private function mapingResponceFields($responce)
    {
        $api_field_map = [
            'txid' => 'txid',
            'category' => 'type',
            'amount' => 'amount',
            'sender' => 'sender',
            'address' => 'receiver',
            'comment' => 'comment',
            'confirmations' => 'confirmations',
        ];
        $result = [];
        foreach ($api_field_map as $key => $val) {
            if (isset($responce["result"]['details'])) {
                if (isset($responce["result"]['details'][0][$key])) {
                    $result[$val] = $responce["result"]['details'][0][$key];
                } else
                    $result[$val] = $responce["result"][$key];
            } else {

                for ($i = 0; $i < count($responce["result"]); $i++) {
                    $result[$i][$val] = $responce["result"][$i][$key];
                }
            }
        }
        return $result;
    }
}